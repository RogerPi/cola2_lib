# Boost
find_package(Boost REQUIRED thread date_time filesystem)
include_directories(${Boost_INCLUDE_DIRS})

# Eigen
find_package(Eigen3 REQUIRED)
set(Eigen3_INCLUDE_DIRS ${EIGEN3_INCLUDE_DIR})
include_directories(${EIGEN3_INCLUDE_DIR})

# Python
find_program(PYTHON "python")
