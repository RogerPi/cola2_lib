
/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_LIB_INCLUDE_COLA2_LIB_IO_TCPSOCKET_H_
#define COLA2_LIB_INCLUDE_COLA2_LIB_IO_TCPSOCKET_H_

#include <boost/asio.hpp>
#include "cola2_lib/io/io_base.h"

#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <boost/thread.hpp>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

namespace cola2
{
namespace io
{
/**
 * TCP config struct
 */
struct TCPConfig
{
  std::string ip;
  unsigned int port;
};

/**
 * \brief Class to handle device connection and data transmission through a TCP socket
 */
class TcpSocket : public IOBase
{
private:
  /**
   * Handler for the boost object that provides TCP socket functionality
   */
  boost::asio::ip::tcp::socket socket_;
  /**
   * Structure to hold the configuration of the TCP socket
   */
  TCPConfig config_;

  void timedReadHandler(boost::optional<boost::system::error_code>* a, const boost::system::error_code& b);

public:
  /**
   * Constructor that configures the TCP socket given the parameters of the TCPConfig structure.
   */
  TcpSocket(cola2::io::TCPConfig);

  ~TcpSocket();

  /**
   * Opens the connection to the TCP socket. config_ parameters must be previously set.
   */
  void open();

  /**
   * Closes the TCP socket connection
   */
  void close();

  /**
   * Reconnects the TCP socket
   */
  void reconnect();

  //*****************************************************************************
  // Read methods
  //*****************************************************************************

  /**
   * Reads a byte from the TCP socket. The read is synchronous if msTimeout is 0,
   * otherwise it is an asynchronous timed read.
   */
  unsigned char readByte(const unsigned int msTimeout);

  /**
   * Reads numOfBytes bytes from the TCP socket and puts them in dataBuffer.
   * The read is synchronous if msTimeout is 0, otherwise it is an asynchronous timed read.
   */
  void read(DataBuffer& dataBuffer, const unsigned int numOfBytes, const unsigned int msTimeout);

  /**
   * Reads bytes from the TCP socket until it finds a delimiter byte and puts them in dataBuffer.
   * The read is synchronous if msTimeout is 0, otherwise it is an asynchronous timed read.
   */
  void readUntil(DataBuffer& dataBuffer, const unsigned char delimiter, const unsigned int msTimeout);

  /**
   * Reads a line from the TCP socket, delimited by lineTerminator.
   * The read is synchronous if msTimeout is 0, otherwise it is an asynchronous timed read.
   */
  std::string readLine(const unsigned int msTimeout, const unsigned char lineTerminator);

  //*****************************************************************************
  // Write methods
  //*****************************************************************************

  /**
   * Writes the dataByte byte to the TCP socket.
   */
  void writeByte(const unsigned char dataByte);

  /**
   * Writes the bytes contained in the dataBuffer DataBuffer to the TCP socket.
   */
  void write(const DataBuffer& dataBuffer);

  /**
   * Writes the bytes contained in the dataString string to the TCP socket.
   */
  void write(const std::string& dataString);
};

}  // namespace io
}  // namespace cola2

#endif  // COLA2_LIB_INCLUDE_COLA2_LIB_COLA2_IO_TCPSOCKET_H_
