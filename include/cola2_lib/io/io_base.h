
/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_LIB_INCLUDE_COLA2_LIB_IO_IOBASE_H_
#define COLA2_LIB_INCLUDE_COLA2_LIB_IO_IOBASE_H_

#include <boost/asio.hpp>
#include <vector>

namespace cola2
{
namespace io
{
/**
 * \brief Base class that provides an interface to manage external connections
 * through serial or ethernet using the same read/write methods.
 */

typedef std::vector<unsigned char> DataBuffer;

class IOBase
{
private:
  boost::asio::io_service io_service_;

public:
  IOBase();

  virtual ~IOBase();

  virtual void open() = 0;

  virtual void close() = 0;

  virtual unsigned char readByte(const unsigned int msTimeout = 0) = 0;

  virtual void read(DataBuffer& dataBuffer, const unsigned int numOfBytes = 0, const unsigned int msTimeout = 0) = 0;

  virtual void readUntil(DataBuffer& dataBuffer, const unsigned char delimiter, const unsigned int msTimeout = 0) = 0;

  virtual std::string readLine(const unsigned int msTimeout = 0, const unsigned char lineTerminator = '\n') = 0;

  virtual void writeByte(const unsigned char dataByte) = 0;

  virtual void write(const DataBuffer& dataBuffer) = 0;

  virtual void write(const std::string& dataString) = 0;

  boost::asio::io_service& get_io_service()
  {
    return io_service_;
  }
};

}  // namespace io
}  // namespace cola2

#endif  // COLA2_LIB_INCLUDE_COLA2_LIB_IO_IOBASE_H_
