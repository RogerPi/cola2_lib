
/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_LIB_INCLUDE_COLA2_LIB_IO_SERIALPORT_H_
#define COLA2_LIB_INCLUDE_COLA2_LIB_IO_SERIALPORT_H_

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time.hpp>
#include <boost/foreach.hpp>
#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>

#include <string>

#include "cola2_lib/io/io_base.h"

namespace cola2
{
namespace io
{
/**
 * Serial port config struct
 */
struct SPConfig
{
  std::string sp_path;
  int sp_baud_rate;
  int sp_char_size;
  int sp_stop_bits;
  std::string sp_parity;
  std::string sp_flow_control;
  int sp_timeout;
};

/**
 * The allowed set of baud rates.
 */
enum EBaudRate
{
  BAUD_50,
  BAUD_75,
  BAUD_110,
  BAUD_134,
  BAUD_150,
  BAUD_200,
  BAUD_300,
  BAUD_600,
  BAUD_1200,
  BAUD_1800,
  BAUD_2400,
  BAUD_4800,
  BAUD_9600,
  BAUD_19200,
  BAUD_38400,
  BAUD_57600,
  BAUD_115200,
  BAUD_230400,
//
// Note: B460800 is defined on Linux but not on Mac OS
//
#ifdef __linux__
  BAUD_460800,
#endif
  BAUD_INVALID,
  BAUD_DEFAULT = BAUD_57600
};

/**
 * The allowed character sizes.
 */
enum ECharacterSize
{
  CHAR_SIZE_5,  // < 5 bit characters.
  CHAR_SIZE_6,  // < 6 bit characters.
  CHAR_SIZE_7,  // < 7 bit characters.
  CHAR_SIZE_8,  // < 8 bit characters.
  CHAR_SIZE_INVALID,
  CHAR_SIZE_DEFAULT = CHAR_SIZE_8
};

/**
 * The allowed numbers of stop bits.
 */
enum EStopBits
{
  STOP_BITS_1,  // 1 stop bit.
  STOP_BITS_2,  // 2 stop bits.
  STOP_BITS_INVALID,
  STOP_BITS_DEFAULT = STOP_BITS_1
};

/**
 * The allowed parity checking modes.
 */
enum EParity
{
  PARITY_EVEN,  // < Even parity.
  PARITY_ODD,   // < Odd parity.
  PARITY_NONE,  // < No parity i.e. parity checking disabled.
  PARITY_INVALID,
  PARITY_DEFAULT = PARITY_NONE
};

/**
 * The allowed flow control modes.
 */
enum EFlowControl
{
  FLOW_CONTROL_HARD,
  FLOW_CONTROL_SOFT,
  FLOW_CONTROL_NONE,
  FLOW_CONTROL_INVALID,
  FLOW_CONTROL_DEFAULT = FLOW_CONTROL_NONE
};

/**
 * \brief Class to handle device connection and data transmission through serial port.
 */

class SerialPort : public IOBase, private boost::noncopyable
{
private:
  /**
   * Handler for the boost object that provides serial port functionality
   */
  mutable boost::asio::serial_port serialPort_;

  /**
   * Structure to hold the configuration of the serial port
   */
  cola2::io::SPConfig config_;

  void timedReadHandler(boost::optional<boost::system::error_code>* a, const boost::system::error_code& b);

public:
  /**
   * Default constructor, initializes the SerialPort object without configuring it.
   */
  SerialPort();

  /**
   * Constructor that configures the serial port given the parameters of the SPConfig structure.
   */
  SerialPort(const SPConfig config);

  /**
   * Opens the connection to the serial port. config_ parameters must be previously set.
   */
  void open();

  /**
   * Opens the connection to the serial port using the port name passed as parameter.
   */
  void open(const std::string& port);

  /**
   * Checks whether the serial port is open or not
   */
  bool isOpen() const;

  /**
   * Closes the serial port connection
   */
  void close();

  /**
   * Changes the current configuration for the given one.
   * @param config serial port configuration.
   * @param do_configure reconfigures the serial port with the given configuration.
   */
  void setConfig(const SPConfig config, bool do_configure = true);

  /**
   * Resets the serial port to fit the current configuration.
   */
  void configure();

  //*****************************************************************************
  // Getters for Serial port configuration settings.
  //*****************************************************************************

  /**
   * Gets the currently configured EBaudRate
   */
  EBaudRate getBaudRate() const;

  /**
   * Gets the currently configured ECharacterSize
   */
  ECharacterSize getCharSize() const;

  /**
   * Gets the currently configured EStopBits
   */
  EStopBits getNumOfStopBits() const;

  /**
   * Gets the currently configured EParity
   */
  EParity getParity() const;

  /**
   * Gets the currently configured EFlowControl
   */
  EFlowControl getFlowControl() const;

  //*****************************************************************************
  // Setters for Serial port configuration settings.
  //*****************************************************************************

  /**
   * Sets the serial port baud rate
   */
  void setBaudRate(const EBaudRate baudRate);
  /**
   * Sets the serial port character size
   */
  void setCharSize(const ECharacterSize charSize);

  /**
   * Sets the serial port number of stop bits
   */
  void setNumOfStopBits(const EStopBits stopBits);

  /**
   * Sets the serial port parity
   */
  void setParity(const EParity parity);

  /**
   * Sets the serial port flow control
   */
  void setFlowControl(const EFlowControl flowControl);

  //********************************************************************************
  // Helper functions to map the configuration parameters to the corresponding Enums
  //********************************************************************************

  /**
   * Gets the EBaudRate corresponding to the integer baudRate parameter
   */
  static EBaudRate baudRateFromInteger(const unsigned int baudRate);
  /**
   * Gets the ECharacterSize corresponding to the integer charSize parameter
   */
  static ECharacterSize charSizeFromInteger(const unsigned int charSize);
  /**
   * Gets the EStopBits corresponding to the integer stopBits parameter
   */
  static EStopBits numOfStopBitsFromInteger(const unsigned int stopBits);
  /**
   * Gets the EParity corresponding to the integer parity parameter
   */
  static EParity parityFromString(const std::string& parity);
  /**
   * Gets the EFlowControl corresponding to the integer flowControl parameter
   */
  static EFlowControl flowControlFromString(const std::string& flowControl);

  //*****************************************************************************
  // Methods to read
  //*****************************************************************************

  /**
   * Reads a byte from the serial port. The read is synchronous if msTimeout is 0,
   * otherwise it is an asynchronous timed read.
   */
  unsigned char readByte(const unsigned int msTimeout = 0);

  /**
   * Reads numOfBytes bytes from the serial port and puts them in dataBuffer.
   * The read is synchronous if msTimeout is 0, otherwise it is an asynchronous timed read.
   */
  void read(DataBuffer& dataBuffer, const unsigned int numOfBytes = 0, const unsigned int msTimeout = 0);

  /**
   * Reads bytes from the serial port until it finds a delimiter byte and puts them in dataBuffer.
   * The read is synchronous if msTimeout is 0, otherwise it is an asynchronous timed read.
   */
  void readUntil(DataBuffer& dataBuffer, const unsigned char delimiter, const unsigned int msTimeout = 0);

  /**
   * Reads a line from the serial port, delimited by lineTerminator.
   * The read is synchronous if msTimeout is 0, otherwise it is an asynchronous timed read.
   */
  std::string readLine(const unsigned int msTimeout = 0, const unsigned char lineTerminator = '\n');

  //*****************************************************************************
  // Methods to write
  //*****************************************************************************

  /**
   * Writes the dataByte byte to the serial port.
   */
  void writeByte(const unsigned char dataByte);

  /**
   * Writes the bytes contained in the dataBuffer DataBuffer to the serial port.
   */
  void write(const DataBuffer& dataBuffer);

  /**
   * Writes the bytes contained in the dataString string to the serial port.
   */
  void write(const std::string& dataString);

  /**
   * Writes the bytes contained in the dataBuffer DataBuffer serial port ensuring that
   * we can write respecting a critic_time speed.
   */
  /*void write(const DataBuffer& dataBuffer, std::string node_name, double critic_time);*/

  /**
   * Sends a break to the serial port.
   */
  void sendBreak();

  //*****************************************************************************
  // Classes to handle Exceptions
  //*****************************************************************************

  /**
   * Class to handle the Exception that is arised when the serial port is not open
   */
  class NotOpen : public std::logic_error
  {
  public:
    NotOpen() : logic_error("Serial port is not open")
    {
    }
    NotOpen(const std::string& whatArg) : logic_error(whatArg)
    {
    }
  };

  /**
   * Class to handle the Exception that is arised when the serial port fails to open
   */
  class OpenFailed : public std::runtime_error
  {
  public:
    OpenFailed() : runtime_error("Serial port failed to open")
    {
    }
    OpenFailed(const std::string& whatArg) : runtime_error(whatArg)
    {
    }
  };

  /**
   * Class to handle the Exception that is arised when the serial port was already open
   */
  class AlreadyOpen : public std::logic_error
  {
  public:
    AlreadyOpen() : logic_error("Serial port was already open")
    {
    }
    AlreadyOpen(const std::string& whatArg) : logic_error(whatArg)
    {
    }
  };

  /**
   * Class to handle the Exception that is arised when trying to configure an unsupported baud rate
   */
  class UnsupportedBaudRate : public std::runtime_error
  {
  public:
    UnsupportedBaudRate() : runtime_error("Unsupported baud rate")
    {
    }
    UnsupportedBaudRate(const std::string& whatArg) : runtime_error(whatArg)
    {
    }
  };

  /**
   * Class to handle the Exception that is arised when the serial port timeout expires while attempting to read
   */
  class ReadTimeout : public std::runtime_error
  {
  public:
    ReadTimeout() : runtime_error("Serial port read timeout")
    {
    }
    ReadTimeout(const std::string& whatArg) : runtime_error(whatArg)
    {
    }
  };
};

}  // namespace io
}  // namespace cola2

#endif  // COLA2_LIB_INCLUDE_COLA2_LIB_IO_SERIALPORT_H_
