/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef _NMEA_PARSER_H_
#define _NMEA_PARSER_H_

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <string>
#include <vector>

/**
 * \brief Class to parse NMEA streams
 * An NMEA stream is a sequence of coma separated strings in this format:
 * $HEADER,field1,field2,field3,...fieldN*CK
 */
class NMEAParser
{
private:
  /**
   * Flag to mark if parsed stream is valid according to its checksum field
   */
  bool valid_;

  /**
   * Array of strings to store each parsed field
   */
  std::vector<std::string> fields_;

  std::string toHex(uint64_t num, int len) const;

public:
  NMEAParser();

  NMEAParser(std::string s);

  /**
   * Splits NMEA string s by comas and puts each field in array of strings (fields_)
   * Returns the fields_ size
   */
  int setFields(std::string s);

  /**
   * Returns the fields_ size
   */
  int size();

  /**
   * Computes checksum for NMEA string str.
   * XOR of all the bytes between the $ and the * (not including the delimiters themselves)
   * and writes the result in hexadecimal
   */
  std::string checksum(std::string str) const;

  /**
   * Gets the contents of field i
   */
  std::string field(int i) const;

  /**
   * Overload of the operator [] to access the contents of field i
   */
  std::string operator[](int i) const;

  /**
   * Sets the contents of the field i to text
   */
  void setField(int i, const std::string& text);

  /**
   * Returns if the parsed NMEA string is valid (according to the checksum)
   */
  bool isValid() const;

  /**
   * Returns the header (first field after the $) of the NMEA string
   */
  std::string header() const;

  /**
   * Returns the full composed NMEA string (including the checksum)
   */
  std::string getSentence() const;
};

#endif  //_NMEA_PARSER_H_
