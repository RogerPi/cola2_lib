/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_PRESSURE_H_
#define COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_PRESSURE_H_

#include <cassert>

namespace cola2
{
namespace utils
{
/**
 * \brief Conversion from pressure in pascals to meters of depth
 * \param[in] pascals Pressure in pascals
 * \param[in] water_density Water density in kg/m^3 in range [900, 1200]
 * \return Meters of depth
 */
double pascalsToMeters(const double pascals, const double water_density);

/**
 * \brief Conversion from meters of depth to pressure in pascals
 * \param[in] meters Meters of depth
 * \param[in] water_density Water density in kg/m^3 in range [900, 1200]
 * \return Pressure in pascals
 */
double metersToPascals(const double meters, const double water_density);

/**
 * \brief Conversion from pressure in bars to pascals
 * \param[in] bars Pressure in bars
 * \return Pressure in pascals
 */
double barsToPascals(const double bars);

/**
 * \brief Conversion from pressure in pascals to bars
 * \param[in] pascals Pressure in pascals
 * \return Pressure in bars
 */
double pascalsToBars(const double pascals);

/**
 * \brief Conversion from pressure in bars to meters of depth
 * \param[in] bars Pressure in bars
 * \param[in] water_density Water density in kg/m^3 in range [900, 1200]
 * \return Meters of depth
 */
double barsToMeters(const double bars, const double water_density);

/**
 * \brief Conversion from meters of depth to pressure in bars
 * \param[in] meters Meters of depth
 * \param[in] water_density Water density in kg/m^3 in range [900, 1200]
 * \return Pressure in bars
 */
double metersToBars(const double meters, const double water_density);
}  // namespace utils
}  // namespace cola2

#endif  // COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_PRESSURE_H_
