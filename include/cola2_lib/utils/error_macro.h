/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef ERROR_MACRO_H
#define ERROR_MACRO_H

#include <iostream>
#include <string>

/**
 * Error macro to throw an exception of the type error_type without error message.
 * \param error_type some instance of a class that can be thrown
 */
#define THROW_NO_MESSAGE(error_type)                                                                                   \
  throw error_type("Runtime error in " + std::string(__FILE__) + std::string(" an line ") + std::to_string(__LINE__));

/**
 * Error macro to throw an exception of the type error_type with the message and adding the file and line where
 * the error was rised.
 * \param error_type some instance of a class that can be thrown
 * \param message the error message
 */
#define THROW_MESSAGE(error_type, message)                                                                             \
  throw error_type(std::string(message) + std::string(" in file: ") + std::string(__FILE__) +                          \
                   std::string(" at ine: ") + std::to_string(__LINE__));

/**
 * Macro that does the macro overloading of the THROW into the two with message and no message
 */
#define RESOLVE_THROW_MACRO(_1, _2, NAME, ...) NAME

/**
 * Overloaded runtime error macro
 */
#define THROW(...) RESOLVE_THROW_MACRO(__VA_ARGS__, THROW_MESSAGE, THROW_NO_MESSAGE)(__VA_ARGS__)

#endif  // ERROR_MACRO_H
