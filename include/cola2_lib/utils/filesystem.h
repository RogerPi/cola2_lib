/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_FILESYSTEM_H_
#define COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_FILESYSTEM_H_

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <boost/filesystem.hpp>
#include <cassert>
#include <cstdlib>
#include <string>

namespace cola2
{
namespace utils
{
/**
 * \brief This function returns true if the given file is accessible and false otherwise
 * \param[in] Path
 * \return Returns true if the given file is accessible and false otherwise
 */
bool isFileAccessible(const std::string&);

/**
 * \brief This function returns true if the given path is a symlink and false otherwise
 * \param[in] Path
 * \return Returns true if the given path is a symlink and false otherwise
 */
bool isSymlink(const std::string&);

/**
 * \brief This function returns the target file of a symlink
 * \param[in] Symlink
 * \return Target file
 */
std::string getSymlinkTarget(const std::string&);

/**
 * \brief This function creates a directory
 * \param[in] Directory path
 */
void createDirectory(const std::string&);

/**
 * \brief This funciton deletes a file
 * \param[in] Target file
 */
void deleteFile(const std::string&);
}  // namespace utils
}  // namespace cola2

#endif  // COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_FILESYSTEM_H_
