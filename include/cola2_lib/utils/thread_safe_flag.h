/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#include <condition_variable>
#include <mutex>

namespace cola2
{
namespace utils
{
class ThreadSafeFlag
{
private:
  std::mutex mtx_;
  std::condition_variable cv_;
  bool state_;

public:
  /**
   * \brief Default constructor. Sets the state to false
   */
  ThreadSafeFlag();

  /**
   * \brief Constructor with the initial state as a parameter
   * \param[in] Initial state
   */
  explicit ThreadSafeFlag(bool);

  /**
   * \brief Set the state to the given value
   * \param[in] State
   */
  void setState(bool);

  /**
   * \brief Returns the state
   * \return State
   */
  bool getState();

  /**
   * \brief This method blocks until the flag is set to the given state
   * \param[in] Desired state
   */
  void blockingWaitFor(bool);

  /**
   * \brief This method blocks until the flag is set to the given state or the timeout expires
   * \param[in] Desired state
   * \param[in] Timeout in seconds
   */
  void timedBlockingWaitFor(bool, double);
};
}  // namespace utils
}  // namespace cola2
