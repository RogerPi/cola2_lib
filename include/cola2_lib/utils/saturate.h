
/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_SATURATE_H_
#define COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_SATURATE_H_

#include <cmath>

namespace cola2
{
namespace utils
{
/**
 * Saturates a value to the range [-min_max, min_max]
 */
double saturate(double x, double min_max);

/**
 * Saturates a value to the range [min_value, max_value]
 */
void saturate(double &value, const double max_value, const double min_value);

}  // namespace utils
}  // namespace cola2

#endif  // COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_SATURATE_H_
