
/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_UNITS_UTIL_H_
#define COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_UNITS_UTIL_H_

#include <cmath>
#include <eigen3/Eigen/Geometry>
#include <iostream>

namespace cola2
{
namespace utils
{
/**
 * Wraps an angle (in radians) between [-PI,PI]
 */
double wrapAngle(const double angle);

/**
 * Wraps an angle (in radians) between [0,2*PI]
 */
double wrapAnglePositive(const double angle);

/**
 * Wraps an angle (in degrees) between [-180.0,180.0]
 */
double wrapAngleDegrees(const double angle_deg);

/**
 * Wraps an angle (in degrees) between [0,360.0]
 */
double wrapAnglePositiveDegrees(const double angle_deg);

//*****************************************************************************
// Angle unit conversions
//*****************************************************************************

/**
 * Converts angle from degrees to radians
 */
double degreesToRadians(const double value);

/**
 * Converts angle from radians to degrees
 */
double radiansToDegrees(const double value);

/**
 * Converts angle from gradians to radians.
 * A gradian is equivalent to 1/400 of a turn, 9/10 of a degree, or PI/200 of a
 * radian.
 */
double gradiansToRadians(const double value);

/**
 * Converts angle from radians to gradians.
 * A gradian is equivalent to 1/400 of a turn, 9/10 of a degree, or PI/200 of a
 * radian.
 */
double radiansToGradians(const double value);

/**
 * Converts degree_minutes (DDMM.MM) to decimal degrees (DD.DD)
 * taking into account the hemisphere passed as a character.
 */
double degreeMinutesToDegrees(const double degree_minutes, const char hemisphere);

/**
 * Converts degree_minutes (DDMM.MM) to decimal degrees (DD.DD)
 * taking into account the hemisphere passed as an integer.
 */
double degreeMinutesToDegreesInt(const double degree_minutes, const int hemisphere);

//*****************************************************************************
// Quaternion-Euler conversions
//*****************************************************************************

/**
 * Get vector of euler angles (roll, pitch, yaw) from rotation matrix
 */
Eigen::Vector3d rotation2euler(const Eigen::Matrix3d &rotation, unsigned int solution_number = 1);

/**
 * Get rotation matrix from vector of euler angles (roll, pitch, yaw)
 */
Eigen::Matrix3d euler2rotation(const Eigen::Vector3d &rpy);

/**
 * Get rotation matrix from vector of euler angles (roll, pitch, yaw)
 */
Eigen::Matrix3d euler2rotation(const double roll, const double pitch, const double yaw);

/**
 * Get vector of euler angles (roll, pitch, yaw) from quaternion
 */
Eigen::Vector3d quaternion2euler(const Eigen::Quaterniond &quat, unsigned int solution_number = 1);

/**
 * Get quaternion matrix from euler angles (roll, pitch, yaw)
 */
Eigen::Quaterniond euler2quaternion(const double roll, const double pitch, const double yaw);
/**
 * Get quaternion matrix from euler angles (roll, pitch, yaw)
 */
Eigen::Quaterniond euler2quaternion(const Eigen::Vector3d &rpy);

//*****************************************************************************
// Rotation derivatives
//*****************************************************************************
/**
 * \brief Compute the differentiation of the rotation matrix over roll
 *
 * \param rpy Euler angles that describe the rotation.
 * \return Rotation matrix differentiated over roll and evaluated
 */
Eigen::Matrix3d d_rotation_d_roll(const Eigen::Vector3d &rpy);

/**
 * \brief Compute the differentiation of the rotation matrix over pitch
 *
 * \param rpy Euler angles that describe the rotation.
 * \return Rotation matrix differentiated over pitch and evaluated
 */
Eigen::Matrix3d d_rotation_d_pitch(const Eigen::Vector3d &rpy);

/**
 * \brief Compute the differentiation of the rotation matrix over yaw
 *
 * \param rpy Euler angles that describe the rotation.
 * \return Rotation matrix differentiated over yaw and evaluated
 */
Eigen::Matrix3d d_rotation_d_yaw(const Eigen::Vector3d &rpy);

//*****************************************************************************
// Cross-product
//*****************************************************************************
/**
 * \brief Compute the cross-product matrix of a given vector.
 *
 * u x v = [u] * v = [v]T * u
 * The cross-product matrix is a skew-symmetry matrix such as MT = -M
 *
 * \param abc Vector that we want to obtain the cross-product matrix
 * \return Cross-product matrix of the vector
 */
Eigen::Matrix3d cross_product_matrix(const Eigen::Vector3d &abc);

}  // namespace utils
}  // namespace cola2

#endif  // COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_UNITS_UTIL_H_
