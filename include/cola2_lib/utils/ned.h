
/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_NAV_NED_H
#define COLA2_NAV_NED_H

#include <cola2_lib/utils/angles.h>
#include <Eigen/Dense>
#include <cmath>

namespace cola2
{
namespace utils
{
const double A = 6378137.0;
const double B = 6356752.3142;
const double ESQ = 6.69437999014 * 0.001;
const double ELSQ = 6.73949674228 * 0.001;
const double F = 1.0 / 298.257223563;

/**
 * \brief Class to represent a North East Down coordinate system and transform
 * to/from it.
 */
class NED
{
private:
  double init_lat_;
  double init_lon_;
  double init_h_;
  double init_ecef_x_;
  double init_ecef_y_;
  double init_ecef_z_;
  Eigen::Matrix3d ecef_to_ned_matrix_;
  Eigen::Matrix3d ned_to_ecef_matrix_;

  double cbrt(const double x) const;

  Eigen::Matrix3d nRe(const double lat_rad, const double lon_rad) const;

public:
  /**
   * \brief NED constructor.
   */
  NED(const double lat, const double lon, const double height);

  /**
   * \brief Convert from geodetic representation (GPS) to ECEF.
   */
  void geodetic2Ecef(const double lat, const double lon, const double height, double &x, double &y, double &z) const;

  /**
   * \brief Convert from ECEF representation to geodetic (GPS).
   */
  void ecef2Geodetic(const double x, const double y, const double z, double &lat, double &lon, double &height) const;

  /**
   * \brief Convert from ECEF representation to NED.
   */
  void ecef2Ned(const double x, const double y, const double z, double &north, double &east, double &depth) const;

  /**
   * \brief Convert from NED representation to ECEF.
   */
  void ned2Ecef(const double north, const double east, const double depth, double &x, double &y, double &z) const;

  /**
   * \brief Convert from geodetic representation (GPS) to NED.
   */
  void geodetic2Ned(const double lat, const double lon, const double height, double &north, double &east,
                    double &depth) const;

  /**
   * \brief Convert from geodetic representation (GPS) to NED.
   */
  Eigen::Vector3d geodetic2Ned(const Eigen::Vector3d &geodetic) const;

  /**
   * \brief Convert from NED representation to geodetic (GPS).
   */
  void ned2Geodetic(const double north, const double east, const double depth, double &lat, double &lon,
                    double &height) const;

  /**
   * \brief Convert from NED representation to geodetic (GPS).
   */
  Eigen::Vector3d ned2Geodetic(const Eigen::Vector3d &ned) const;

  /**
   * \brief Get initial latitude.
   */
  double getInitLatitude() const;

  /**
   * \brief Get initial longitude.
   */
  double getInitLongitude() const;
};
}  // namespace utils
}  // namespace cola2

#endif  // COLA2_NAV_NED_H
