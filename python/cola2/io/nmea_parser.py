#!/usr/bin/env python
# Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.


class NMEAParser():
    '''Class to parse NMEA streams. An NMEA stream is a sequence of coma
       separated strings in this format:
       $HEADER,field1,field2,field3,...fieldN*CK'''

    def __init__(self, data=""):
        '''Constructor. Data field is optional'''
        self.valid = False
        self.fields = []
        self.hex_lst = list("0123456789ABCDEF")
        if data != "":
            self.setFields(data)

    def setFields(self, data):
        '''Splits NMEA string s by comas and puts each field in array of
           strings (self.fields). Returns the self.fields lenght'''
        self.fields = []
        try:
            t = data.strip()
            if t[0] == '$':
                self.valid = True
                idx = t.find('*')
                if idx > 0:
                    if idx + 3 == len(t):
                        if t[(idx + 1):] != self.checksum(t):
                            self.valid = False  # Invalid checksum
                    t = t[:idx]
                self.fields = t.split(',')
        except:
            self.valid = False
        return len(self.fields)

    def checksum(self, data):
        '''Computes checksum for an NMEA string. XOR of all the bytes between
           the $ and the * (not including the delimiters themselves) and writes
           the result in hexadecimal'''
        data_list = list(data)
        chk = 0
        index = 1
        while index < len(data) and data[index] != '*':
            chk = chk ^ ord(data_list[index])  # Bitwise XOR
            index += 1
        return self.toHex(chk, 2)

    def field(self, i):
        '''Gets the contents of field i'''
        if len(self.fields) > i:
            return self.fields[i]
        return ""

    def setField(self, i, text):
        '''Sets the contents of the field i to text'''
        while len(self.fields) <= i:
            self.fields.append("")
        self.fields[i] = text

    def header(self):
        '''Returns the header (first field after the $) of the NMEA string'''
        return self.field(0)

    def size(self):
        '''Returns the amount of fields (length of self.fields)'''
        return len(self.fields)

    def isValid(self):
        '''Returns if the parsed NMEA string is valid (according to the
           checksum)'''
        return self.valid

    def toHex(self, num, lenght):
        '''Convert from the num integer to an hex string of a given length'''
        s = ["0"] * lenght
        for i in range(lenght - 1, -1, -1):
            tr = num & 0x0F
            s[i] = self.hex_lst[tr]
            num = num >> 4
        return "".join(s)

    def getSentence(self):
        ''' Compose sentence joining all fields with comas in between and adds
            also the checksum at the end '''
        if len(self.fields) == 0:
            return ""
        s = ','.join(self.fields)
        return s + "*" + self.checksum(s) + "\r\n"
