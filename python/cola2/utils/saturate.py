#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.

import math
import numpy as np

def saturate_vector(v, min_max) :
    ret = np.zeros( len(v) )
    for i in range( len(v) ) :
        if v[i] < -min_max[i] : ret[i] = -min_max[i]
        elif v[i] > min_max[i] : ret[i] = min_max[i]
        else : ret[i] = v[i]
    return ret


def saturate_value(v, min_max) :
    ret = np.zeros( len(v) )
    for i in range( len(v) ) :
        if v[i] < -min_max : ret[i] = -min_max
        elif v[i] > min_max : ret[i] = min_max
        else : ret[i] = v[i]
    return ret


def saturate_value_float(v, min_max):
    if v > min_max:
        v = min_max
    elif v < -min_max:
        v = -min_max

    return v

def test() :
    print "Saturate_vector:"
    print str(saturate_vector([1.8,0.3,-3.2, -0.7], np.ones(4)))

    print "Saturate_value:"
    print str(saturate_value([1.8,0.3,-3.2, -0.7], 1.0))

    print "Saturate_value float:"
    print str(saturate_value_float(1.8, 1.0))

if __name__ == '__main__':
    test()
