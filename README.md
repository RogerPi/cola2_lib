# COLA2 LIB

Reusable code that is used all over the COLA2 software architecture.

## Compiling/installing

To complie this project:

```bash
mkdir build
cd build
cmake ..
make
sudo make install
```

Installing the library is only mandatory for python purposes, the c++ library and headers can be found using cmake regardless of this being installed.

## C++ compiling and linking against this library

This library can be reused from other c++ libraries. This library provides cmake files to ease this process. For this purpose, add the following to your cmake file:

```bash
find_package(COLA2_LIB REQUIRED)
include_directories(${COLA2_LIB_INCLUDE_DIRS})

# Add your targets
target_link_library(my_target ${COLA2_LIB_LIBRARIES})
```

If the library is not installed (i.e. `sudo make install` has not been exectued) this commands should work as well. If your package cannot find this library, you can help it by defing the library path in your own project cmake running: `cmake  -DCOLA2_LIB_DIR=path_to_the_build_folder ..`.

## Python usage

This library also provides python functionalities. For these to be usable the pakage has to be installed with `sudo make install`. Here, the following packages are provided: cola2_lib, cola2_lib.utils, cola2_lib.utils.angles, cola2_lib.utils.ned, cola2_lib.utils.saturate, cola2_lib.io.nmea_parser.
